Plantilla para microcontroladores Espressif
===========================================

![ESP32_C3](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEiFJIPwClGlVL8t41KsgQLeKwHpMVYE35igKZGVcQ3W94Dr7itYbCxt_WjzsIbxCZiRywpjAgG98f4vaO8ovDhVTpFBoJSpRZukF-ZN5i039sI7qr-TcXLObnKxG34x2jFObNXT4gJzdV94VfRuBzoSGmJYOTkD1aTYBkayfA-8Hv00SA2hEa7S37JPLNkJ/s1601/20230705_0001.png)

La plantilla está hecha para el IDE VSCode, y utiliza Docker para tener todo lo necesario en un contenedor y no tener que instalar y configurar todas las herramientas y plugins en el ordenador de desarrollo.

En ella se encuentra un firmware que consiste en:
- Hilo principal que muestra mensajes de logs.
- Hilo "blinker" que enciende y apaga un led. Inluyendo su estado en el log.
- Hilo "contador" que va incrementando una variable e imprimiendo su valor.
- Un temporizador de FreeRTOS "rgb_timer" que apaga y enciende el led RGB de la placa.
- Un temporizador de FreeRTOS "sleep_timer" que pone al microcontrolador en modo reposo y lo despierta pasado 1 minuto.
- Un temporizador de Esp (por hardware) "espTimerHandle" que pausa el timer "rgb_timer" cada 5 segundos.
- La variable contador se encuentra en la memoria RTC, para guardar su valor durante deep sleep.
- Macros personalizadas para los logs. Detectan el uso de SystemView y usa sus funciones nativas para los logs, en caso contrario usan la librería estándar log de Esp-Idf.


Instalación
-----------
Basta con clonar el repositorio y ejecutar en la terminal dentro de la carpeta del proyecto lo siguiente:

docker run --rm -v $PWD:/project -w /project espressif/idf:v5.0.2

Esto bajará la imagen desde el repositorio de Espressif y ejecutará los comandos del contenedor.
En caso de cambiar de microcontrolador o de versión de idf, modificar los archivos devcontainer.json, Dockerfile y settings.json.


Captura de datos
----------------
Para capturar datos y poder mostrarlos en SystemView hay que realizar los siguientes pasos:

- Abrir openOCD server
- Conectarse por telnet. Comando: telnet localhost 4444
- Resetear el micro. Comando: reset run
- Empezar a capturar. Comando: esp sysview start file://Trace.SVDat  1 -1 -1
- Terminar la captura. Comando: esp sysview stop

Se generará el archivo Trace.SVDat, que se puede abrir con SystemView.


Características
---------------

![VSCode](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhlZaQbA7kRC7podZFuKyPa5DR_J-vrwrsnLxnjUDs-G_XSDlUtBKYVxnxqYn0ZSkgfp74_gl5iNqH5XyrkyMX28D1icPN9xdaHufAhR272hkB7x4e9_lEFI4MXM1hKbWK46OqG6eWKkdEjyC9LADn2tNJLaYffN0YPNPXyCzojSoPshEVg-a4icgGfixtO/s3440/Captura%20de%20pantalla%20de%202023-06-20%2012-58-15.png)

Las características de la plantilla son las siguientes:

- Utiliza una imagen oficial de Espressif (basada en Ubuntu) con idf v. 5.0.2
- Añade la consola zsh al entorno linux.
- Añade el editor nano para edición de ficheros.
- Añade la aplicación telnet para conectarnos a OpenOCD.
- Se descarga y sustituye OpenOCD v.0.11 por la v.0.12 (necesario para el C3).
- Selecciona el lenguaje UTF-8 para prevenir un error en la imagen oficial.
- Añade al usuario al grupo plugdev para poder acceder a los dispositivos por USB.
- Instala los plugins: C++, Espressif IDF, MemoryView, RTOS Views, C Unity Test Explorer, Cpp Check Lint, Doxygen Documentation Generation, psioniq File Header, TODO Tree, Dash, Doxigen y Log File
  Highlighter.
- Da privilegios para conectar el contenedor a un debugger JTAG por el puerto serie/USB.
- Tiene configurados los plugins instalados.
- Añade un perfil debug para GDB al de Esp-Ifd ya existente.
- Añade algunas tareas, como lanzar SystemView, examinar el tamaño del binario generado, mostrar los tamaños por componentes o comparar las diferencias de tamaño entre dos binarios o "builds".

Para más información consultar el artículo en el [blog.](https://www.circuiteando.net/2023/06/plantilla-para-microcontroladores_30.html)
