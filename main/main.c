/*
 * File: main.c
 * Project: __PROJECT_NAME__
 * Created Date: Friday, May 6th 2022, 3:04:42 pm
 * Author: __AUTHOR__
 * e-mail: __EMAIL__
 * web: __WEB_PAGE__
 *
 * MIT License
 *
 * Copyright (c) 2023 Your Company
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Copyright (c) 2023 __AUTHOR__
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: __AUTHOR__
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     __AUTHOR__      xxxx <--!!!!!!!!
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "led_strip.h"
#include "esp_timer.h"
#include "esp_sleep.h"
#include "esp_wifi.h"
#include "log_conf.h"

// Definiciones de prioridades de tareas
#define BLINK_TASK_PRIORITY 2
#define CONTADOR_TASK_PRIORITY 2

#define DEFAULT_STACK_SIZE 2048

static const char *TAG = "Main";

// Usada exclusivamente por el hilo 'Contador'. Para usar un datawatch.
RTC_DATA_ATTR uint32_t Contador = 0;

static StaticTask_t BlinkTCBBuffer;
static StackType_t BlinkStackBuffer[DEFAULT_STACK_SIZE];

static StaticTask_t ContadorTCBBuffer;
static StackType_t ContadorStackBuffer[DEFAULT_STACK_SIZE];

static StaticTimer_t rgbTimerBuffer;
static StaticTimer_t sleepTimerBuffer;

static led_strip_handle_t ledRGB;

// FIXME: Prueba, tiene que aparecer en tasks.
// TODO: Prueba, tiene que aparecer en tasks.

/**
 * Hilo encargado de hacer parpadear un led externo en GPIO 3 cada 2 segundos.
 */
static void blink()
{
    const char *TAG = "Blink";
    const gpio_num_t led = GPIO_NUM_3;
    const gpio_config_t ledConfig = {
        .pin_bit_mask = (1 << led),
        .mode = GPIO_MODE_OUTPUT,
        .pull_up_en = 0,
        .pull_down_en = 0,
        .intr_type = GPIO_INTR_DISABLE};

    gpio_config(&ledConfig);

    while (true)
    {
        gpio_set_level(led, 0);
        LOGI(TAG, "Led: Off");
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        gpio_set_level(led, 1);
        LOGI(TAG, "Led: On");
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}

/**
 * Hilo encargado de contar valores para poder verlos durante los modos de
 * depuración.
 */
static void contador()
{
    const char *TAG = "Contador";
    static uint32_t Contador2 = 0;
    // Para sacar la dirección de memoria de la variable (hex). No se ve
    // a menos que sean globales. También se puede buscar en el archivo .map

    LOGIF(TAG, "Address Contador2: %p", &Contador2);

    while (pdTRUE)
    {
        Contador++;
        Contador2 += 4;

        LOGIF(TAG, "Cont: %u", (uint)Contador);
        LOGIF(TAG, "Cont2: %u", (uint)Contador2);

        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

/**
 * Configura el led RGB conectado al pin GPIO 8, utilizando el módulo RMT
 * por hardware.
 */
static void configure_led(void)
{

    led_strip_config_t stripConfig = {
        .strip_gpio_num = GPIO_NUM_8,
        .max_leds = 1,
    };
    led_strip_rmt_config_t rmtConfig = {
        .resolution_hz = 10 * 1000 * 1000, // 10MHz
    };
    ESP_ERROR_CHECK(led_strip_new_rmt_device(&stripConfig, &rmtConfig, &ledRGB));
    led_strip_clear(ledRGB);
}

/**
 * Hace parpadear el red RBG de la placa.
 *
 * @param pxTimer Identificador del timer de FreeRTOS que ejecuta esta función.
 */
static void rgb_blink(TimerHandle_t pxTimer)
{
    static bool state = pdFALSE;

    // Para comprobar que guarda el valor durante los modos de ahorro de energía.
    if (Contador > 70 && Contador < 90)
    {
        led_strip_set_pixel(ledRGB, 0, 0, 254, 0); // Verde
        led_strip_refresh(ledRGB);
        return;
    }

    if (state)
    {
        led_strip_set_pixel(ledRGB, 0, 10, 10, 100);
        LOGD(TAG, "RGB timer: BLUE led");
    }
    else
    {
        led_strip_set_pixel(ledRGB, 0, 100, 10, 10);
        LOGD(TAG, "RGB timer: RED led");
    }

    led_strip_refresh(ledRGB);
    state = !state;
}

/**
 * Para y arranca el rgb_timer según el tiempo especificado por otro timer (temporizador),
 * en este caso un esp_timer. A diferencia de los de FreeRTOS, este utiliza un
 * timer por hardware de 52 bits con micro segundos de resolución.
 *
 * @param arg Puntero con el manejador (handler) de un timer de FreeRTOS.
 */
static void esp_timer_callback(void *arg)
{
    TimerHandle_t *pTimer = (TimerHandle_t *)arg;
    if (xTimerIsTimerActive(*pTimer) == pdTRUE)
    {
        xTimerStop(*pTimer, 0);
        LOGD(TAG, "RGB timer stopped");
    }
    else
    {
        xTimerStart(*pTimer, 0);
        LOGD(TAG, "RGB timer started");
    }
}

static void sleep(TimerHandle_t pxTimer)
{
    led_strip_clear(ledRGB); // Apaga el led RGB.
    LOGD(TAG, "Sleep Now");
    // Para light sleep
    // ESP_ERROR_CHECK(esp_sleep_enable_timer_wakeup(60 * 1000 * 1000)); // 60 segundos.
    // esp_light_sleep_start();
    // Para deep sleep
    esp_deep_sleep(60 * 1000 * 1000);
    LOGD(TAG, "Wake Up");
}

void app_main(void)
{

#if CONFIG_APPTRACE_SV_ENABLE
    while (!SEGGER_SYSVIEW_Started()) // Espera a que se soliciten datos.
    {
        vTaskDelay(1);
    }
    esp_log_set_vprintf(esp_sysview_vprintf); // Redirige el resto de logs a systemview.
#endif

    LOGI(TAG, "Board init.");
    esp_wifi_stop();

    TaskHandle_t blinkHandle = NULL;
    blinkHandle = xTaskCreateStatic(blink,               // Función a ejecutar.
                                    "blinker",           // Nombre para ayudar durante la depuración de errores.
                                    DEFAULT_STACK_SIZE,  // Tamaño del buffer de la pila (en bytes).
                                    NULL,                // Parámetros pasados a la tarea.
                                    BLINK_TASK_PRIORITY, // Prioridad de la tarea.
                                    BlinkStackBuffer,    // Buffer a usar para la pila.
                                    &BlinkTCBBuffer);    // Variable que contiene el TCB de la tarea.

    if (blinkHandle == NULL)
    {
        LOGE(TAG, "Task blink creation failed!");
        while (1)
            ;
    }

    TaskHandle_t contadorHandle = NULL;
    contadorHandle = xTaskCreateStatic(contador,
                                       "contador",
                                       DEFAULT_STACK_SIZE,
                                       NULL,
                                       CONTADOR_TASK_PRIORITY,
                                       ContadorStackBuffer,
                                       &ContadorTCBBuffer);
    if (contadorHandle == NULL)
    {
        LOGE(TAG, "Task contador creation failed!");
        while (1)
            ;
    }

    TimerHandle_t rgbTimerHandle = xTimerCreateStatic("rgb_timer",
                                                      500 / portTICK_PERIOD_MS,
                                                      pdTRUE,
                                                      (void *)1,
                                                      rgb_blink,
                                                      &rgbTimerBuffer);

    if (rgbTimerHandle == NULL)
    {
        LOGE(TAG, "Timer rgbTimer creation failed!");
        while (1)
            ;
    }

    TimerHandle_t sleepTimerHandle = xTimerCreateStatic("sleep_timer",
                                                        60000 / portTICK_PERIOD_MS,
                                                        pdTRUE,
                                                        (void *)2,
                                                        sleep,
                                                        &sleepTimerBuffer);

    if (sleepTimerHandle == NULL)
    {
        LOGE(TAG, "Timer sleep creation failed!");
        while (1)
            ;
    }

    esp_timer_handle_t espTimerHandle;
    esp_timer_create_args_t espTimerArgs = {
        .callback = &esp_timer_callback,
        .arg = (void *)&rgbTimerHandle,
        .name = "espTimer"};

    ESP_ERROR_CHECK(esp_timer_create(&espTimerArgs, &espTimerHandle));

    configure_led();
    xTimerStart(rgbTimerHandle, 0);
    //  xTimerStart(sleepTimerHandle, 0); // Para inicial el deep sleep.
    ESP_ERROR_CHECK(esp_timer_start_periodic(espTimerHandle, 5 * 1000000)); // Cada 5 segundos.

    while (pdTRUE)
    {
        for (int i = 10; i >= 0; i--)
        {
            LOGE(TAG, "Test ERROR message");
            LOGW(TAG, "Test WARNING message");
            LOGI(TAG, "Test INFO message");
            LOGD(TAG, "Test DEBUG message");

            vTaskDelay(10 / portTICK_PERIOD_MS);
        }
        vTaskDelay(10000 / portTICK_PERIOD_MS);
    }
}
