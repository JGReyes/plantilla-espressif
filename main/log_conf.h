/*
 * File: sysview_conf.h
 * Project: __PROJECT_NAME__
 * Created Date: Wednesday, June 7th 2023, 10:40:04 am
 * Author: __AUTHOR__
 * e-mail: __EMAIL__
 * web: __WEB_PAGE__
 *
 * MIT License
 *
 * Copyright (c) 2023 Your Company
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Copyright (c) 2023 __AUTHOR__
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: __AUTHOR__
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     __AUTHOR__      xxxx <--!!!!!!!!
 */

#ifndef LOG_CONF_H_
#define LOG_CONF_H_

#include "esp_log.h"

// Log personalizado para poder elegir entre SystemView y el de Esp.
// Habilita SystemView y las macros para el envio de mensajes.
// Se recomienda compilar en Release para un mejor rendimiento de SystemView.

#if CONFIG_APPTRACE_SV_ENABLE
#include "esp_sysview_trace.h"

#define svPrint(x) SEGGER_SYSVIEW_Print(x)
#define svPrintfTarget(x, args...) SEGGER_SYSVIEW_PrintfTarget(x, args)
#define svWarn(x) SEGGER_SYSVIEW_Warn(x)
#define svWarnfTarget(x, args...) SEGGER_SYSVIEW_WarnfTarget(x, args)
#define svError(x) SEGGER_SYSVIEW_Error(x)
#define svErrorfTarget(x, args...) SEGGER_SYSVIEW_ErrorfTarget(x, args)
#define svEnterISR() SEGGER_SYSVIEW_RecordEnterISR()
#define svExitISR() SEGGER_SYSVIEW_RecordExitISR()
#define svStart() SEGGER_SYSVIEW_Start()
#define svStop() SEGGER_SYSVIEW_Stop()
#define svEnableEvents() SEGGER_SYSVIEW_EnableEvents()
#define svDisableEvents() SEGGER_SYSVIEW_DisableEvents()

#define LOGEF(x, m, args...) svErrorfTarget(m, args)
#define LOGWF(x, m, args...) svWarnfTarget(m, args)
#define LOGIF(x, m, args...) svPrintfTarget(m, args)
#define LOGDF(x, m, args...) svPrintfTarget("Debug: " m, args)
#define LOGE(x, m) svError(m)
#define LOGW(x, m) svWarn(m)
#define LOGI(x, m) svPrint(m)
#define LOGD(x, m) svPrint("Debug: " m)

#else

#define LOGEF(x, m, args...) ESP_LOGE(x, m, args)
#define LOGWF(x, m, args...) ESP_LOGW(x, m, args)
#define LOGIF(x, m, args...) ESP_LOGI(x, m, args)
#define LOGDF(x, m, args...) ESP_LOGD(x, m, args)
#define LOGE(x, m) ESP_LOGE(x, m)
#define LOGW(x, m) ESP_LOGW(x, m)
#define LOGI(x, m) ESP_LOGI(x, m)
#define LOGD(x, m) ESP_LOGD(x, m)

// Se eliminan las llamadas a funciones de SystemView cuando no se utiliza.
#define svPrint(x) ((void)0)
#define svPrintfTarget(x, args...) ((void)0)
#define svWarn(x) ((void)0)
#define svWarnfTarget(x, args...) ((void)0)
#define svError(x) ((void)0)
#define svErrorfTarget(x, args...) ((void)0)
#define svEnterISR() ((void)0)
#define svExitISR() ((void)0)
#define svStart() ((void)0)
#define svStop() ((void)0)
#define svEnableEvents() ((void)0)
#define svDisableEvents() ((void)0)
#define svWaitConn() ((void)0)

#endif

#endif /* LOG_CONF_H_ */
